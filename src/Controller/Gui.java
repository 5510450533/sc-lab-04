package Controller;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Gui extends JFrame {
	private JPanel frame,frame2;
	private JFrame f1,f2;
	private JComboBox<String> jcombo;
	private JTextField jTextCount;
	private JButton button;
	private JLabel count,select;
	private Control c;
	private JTextArea textArea;
	private String result = "";
	public Gui(){
		c = new Control();
		frame = new JPanel();
		frame2 = new JPanel();
		textArea = new JTextArea();
		JScrollPane scroll = new JScrollPane (textArea);
		
		f1 = new JFrame();
		f2 = new JFrame();
		
		jcombo = new JComboBox<String>();
		jcombo.setBounds(110, 30, 250, 30);
		frame.add(jcombo);
		
		jTextCount = new JTextField();
		jTextCount.setBounds(100, 85, 200, 30);
		frame.add(jTextCount);
		
		button = new JButton("Run");
		button.setBounds(0, 127, 395, 40);
		frame.add(button);
		
		count = new JLabel("Count: ");
		count.setBounds(50, 75, 50, 50);
		frame.add(count);
		
		select = new JLabel("Choose: ");
		select.setBounds(50, 20, 100, 50);
		frame.add(select);
		
		jcombo.addItem("     Pattern 1");
		jcombo.addItem("     Pattern 2");
		jcombo.addItem("     Pattern 3");
		jcombo.addItem("     Pattern 4");
		jcombo.addItem("     Pattern 5");

		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				result+=c.outPut(jcombo.getSelectedIndex(),jTextCount.getText());
				textArea.setText(result);
			}
		});
		
		frame.setLayout(null);
//		add(frame);
		
		f1.add(frame);
		f2.add(frame2);
		
		f1.setVisible(true);
		f2.setVisible(true);
		
		f1.setSize(400,200);
		f1.setLocation(500,200);
		f1.setResizable(false);
		
		f2.setSize(400,400);
		f2.setLocation(0,0);

		
		f2.add(scroll);
	}
}
