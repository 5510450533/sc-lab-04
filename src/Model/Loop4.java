package Model;



public class Loop4 {
	private int x;
	public Loop4(int x)
	{
		this.x = x;
	}
	
	@Override
	public String toString() 
	{
		 String r = "";
		for (int i = 1 ; i <= x ; i++)
		{
			for (int j = 1 ; j <= x ; j++)
			{
				if (j % 2 == 0)
					r = r + "*";
				else
					r = r + "-";
			}
				r = r + "\n";
		}
		return r ;
	}
}
