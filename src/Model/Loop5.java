package Model;



public class Loop5 {
	private int x;
	public Loop5(int x)
	{
		this.x = x;
	}
	
	@Override
	public String toString() 
	{
		 String r = "";
		for (int i = 1 ; i <= x ; i++)
		{
			for (int j = 1 ; j <= x ; j++)
			{
				if ((i + j) % 2 == 0)
					r = r + "*";
				else
					r = r + " ";
			}
				r = r + "\n";
		}
		return r ;
	}
}
